//
//  VGTreeDetailsVC.m
//  vgtreedbinput
//
//  Created by Steven Kim on 1/24/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGTreeDetailsVC.h"
#import <CoreLocation/CoreLocation.h>

@interface VGTreeDetailsVC () <UIScrollViewDelegate, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *labelSite;
@property (weak, nonatomic) IBOutlet UILabel *labelCourt;
@property (weak, nonatomic) IBOutlet UILabel *labelBotanical;
@property (weak, nonatomic) IBOutlet UILabel *labelCommon;

@property (weak, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (weak, nonatomic) IBOutlet UILabel *latitudeLabel;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, strong) UIImagePickerController *cameraPC;
@property (weak, nonatomic) IBOutlet UIImageView *pictureIV;

@end

@implementation VGTreeDetailsVC

@synthesize vgapi = _vgapi;
@synthesize tree = _tree;

@synthesize labelSite = _labelSite;
@synthesize labelCourt = _labelCourt;
@synthesize labelBotanical = _labelBotanical;
@synthesize labelCommon = _labelCommon;

@synthesize longitudeLabel = _longitudeLabel;
@synthesize latitudeLabel = _latitudeLabel;

@synthesize locationManager = _locationManager;
@synthesize cameraPC = _cameraPC;
@synthesize pictureIV = _pictureIV;

#pragma mark - View management

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    NSLog(@"tree: %@", self.tree);
    
    self.labelSite.text = [self.tree valueForKey:@"site"];
    self.labelCourt.text = [self.tree valueForKey:@"court"];
    self.labelBotanical.text = [self.tree valueForKey:@"botanical"];
    self.labelCommon.text = [self.tree valueForKey:@"common"];
    
    self.longitudeLabel.text = [self.tree valueForKey:@"longitude"];
    self.latitudeLabel.text = [self.tree valueForKey:@"latitude"];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;

    BOOL cameraIsAvailable = NO;
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
        cameraIsAvailable = YES;
    }

    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
        self.cameraPC = [[UIImagePickerController alloc] init];
        self.cameraPC.sourceType = UIImagePickerControllerSourceTypeCamera;
        self.cameraPC.delegate = self;
    }
    
    self.scrollView.delegate = self;
    self.scrollView.contentSize = CGSizeMake(320, 568);
}

#pragma mark - Location Manager delegate

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [self.locationManager stopUpdatingLocation];
    self.longitudeLabel.text = [NSString stringWithFormat:@"%f", newLocation.coordinate.longitude];
    self.latitudeLabel.text = [NSString stringWithFormat:@"%f", newLocation.coordinate.latitude];
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
	NSLog(@"Error: %@", [error description]);
}

#pragma mark - Image Picker delegate

- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo
{
    [self dismissModalViewControllerAnimated:NO];    
    self.pictureIV.image = image;
}

- (void)viewDidUnload {
    [self setPictureIV:nil];
    [self setScrollView:nil];
    [super viewDidUnload];
}

#pragma mark - Other methods

- (IBAction)getLocation:(id)sender {
    [self.locationManager startUpdatingLocation];
}

- (IBAction)getCameraPicture:(id)sender {
    [self presentModalViewController:self.cameraPC animated:YES];
}

- (IBAction)submitTree:(id)sender {
    
    [self.vgapi setTreeLocation:[[self.tree valueForKey:@"tree_id"] intValue] withLongitude:[self.longitudeLabel.text doubleValue] withLatitude:[self.latitudeLabel.text doubleValue]];
    
    [self.vgapi setTreePhoto:[[self.tree valueForKey:@"tree_id"] intValue] withPhoto:self.pictureIV.image];
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
