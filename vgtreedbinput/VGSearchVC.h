//
//  VGSearchVC
//  vgtreedbinput
//
//  Created by Steven Kim on 3/17/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VGApi.h"

@interface VGSearchVC : UIViewController
@property (strong, nonatomic) VGApi *vgapi;
@end
