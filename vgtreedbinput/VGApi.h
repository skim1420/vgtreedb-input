//
//  VGApi.h
//  vgtreedbinput
//
//  Created by Steven Kim on 1/24/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VGApi : NSObject

- (NSArray *)getCourts;
- (NSArray *)getUpdatableTreesForCourt:(NSString *)court;
- (void)setTreeLocation:(int)treeId withLongitude:(float)longitude withLatitude:(float)latitude;
- (void)setTreePhoto:(int)treeId withPhoto:(UIImage *)photo;
- (NSDictionary *)getTreeBySite:(NSString *)site;

@end
