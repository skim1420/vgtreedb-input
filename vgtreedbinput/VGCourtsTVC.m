//
//  VGCourtsTVC.m
//  vgtreedbinput
//
//  Created by Steven Kim on 1/24/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGCourtsTVC.h"
#import "VGTreesTVC.h"
#import "VGSearchVC.h"

@interface VGCourtsTVC ()

@property (strong, nonatomic) VGApi *vgapi;
@property (strong, nonatomic) NSArray *courts;

@end

@implementation VGCourtsTVC

@synthesize vgapi = _vgapi;
@synthesize courts = _courts;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.vgapi = [[VGApi alloc] init];
}

- (void)viewWillAppear:(BOOL)animated {
    self.courts = [self.vgapi getCourts];
    [self.tableView reloadData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"Segue Trees"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        NSString *court = [[self.courts objectAtIndex:indexPath.row] valueForKey:@"court"];
        [segue.destinationViewController setCourt:court];
        [segue.destinationViewController setVgapi:self.vgapi];
    }
    if ([[segue identifier] isEqualToString:@"Search"]) {
        [segue.destinationViewController setVgapi:self.vgapi];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.courts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Court Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];

    NSString *courtName = [[self.courts objectAtIndex:indexPath.row] valueForKey:@"court"];
    if ([courtName isEqualToString:@""]) {
        courtName = @"(Unspecified)";
    }
    cell.textLabel.text = courtName;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ trees", [[self.courts objectAtIndex:indexPath.row] valueForKey:@"count"]];
    
    return cell;
}

@end
