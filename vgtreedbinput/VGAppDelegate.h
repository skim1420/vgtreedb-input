//
//  VGAppDelegate.h
//  vgtreedbinput
//
//  Created by Steven Kim on 1/22/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
