//
//  VGApi.m
//  vgtreedbinput
//
//  Created by Steven Kim on 1/24/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGApi.h"
#import "NSString+URLEncoding.h"

#define apikey @"vgtreedb"

#define FULLSIZE 640 // 320px @2x
#define FULLQUALITY 0.75

#define THUMBSIZE 120 // 60px @2x
#define THUMBQUALITY 0.5

@implementation VGApi

- (NSArray *)getCourts {

    NSArray *courts = nil;
    NSString *urlstring = @"http://www7.pairlite.com/pl1705/x/vgtreedb/trees.php?method=getCourts&status=open";
    NSURL *url = [NSURL URLWithString:urlstring];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:apikey forHTTPHeaderField:@"Authorization"];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (responseData != nil) {
        //NSLog(@"getCourts response: %i\n%@", response.statusCode, [NSString stringWithCString:[responseData bytes] encoding:NSUTF8StringEncoding]);
        courts = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    }
    return courts;
}

- (NSArray *)getUpdatableTreesForCourt:(NSString *)court {
    
    NSArray *trees = nil;
    NSString *urlstring = [NSString stringWithFormat:@"http://www7.pairlite.com/pl1705/x/vgtreedb/trees.php?method=getTrees&status=open&court=%@", [court encodedURLString]];
    NSURL *url = [NSURL URLWithString:urlstring];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:apikey forHTTPHeaderField:@"Authorization"];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (responseData != nil) {
        //NSLog(@"getCourts response: %i\n%@", response.statusCode, [NSString stringWithCString:[responseData bytes] encoding:NSUTF8StringEncoding]);
        trees = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    }
    
    // sort by tree type
    NSMutableArray *treeSorter = [trees mutableCopy];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"common"  ascending:YES];
    [treeSorter sortUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    trees = [treeSorter copy];

    return trees;
}

- (void)setTreeLocation:(int)treeId withLongitude:(float)longitude withLatitude:(float)latitude {
    
    NSString *urlstring = [NSString stringWithFormat:@"http://www7.pairlite.com/pl1705/x/vgtreedb/trees.php?method=setTreeLocation&tree_id=%i&longitude=%f&latitude=%f", treeId, longitude, latitude];
    NSURL *url = [NSURL URLWithString:urlstring];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:apikey forHTTPHeaderField:@"Authorization"];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
}

- (void)setTreePhoto:(int)treeId withPhoto:(UIImage *)photo {
    
    NSString *urlString = @"http://www7.pairlite.com/pl1705/x/vgtreedb/trees.php?method=uploadTreePicture";
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request addValue:apikey forHTTPHeaderField:@"Authorization"];

    NSString *boundary = @"---------------------------Boundary Line---------------------------";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];

    CGSize currentSize = [photo size];
    CGSize thumbSize;
    CGSize fullSize;
    
    if (currentSize.width > currentSize.height) {
        thumbSize = CGSizeMake( (THUMBSIZE/currentSize.height)*currentSize.width , THUMBSIZE );
        fullSize = CGSizeMake( (FULLSIZE/currentSize.height)*currentSize.width , FULLSIZE );
    } else {
        thumbSize = CGSizeMake( THUMBSIZE , (THUMBSIZE/currentSize.width)*currentSize.height );
        fullSize = CGSizeMake( FULLSIZE , (FULLSIZE/currentSize.width)*currentSize.height );
    }
    
    // thumbnail
    UIGraphicsBeginImageContext(thumbSize);
    [photo drawInRect:CGRectMake(0,0,thumbSize.width,thumbSize.height)];
    UIImage* resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    NSData *imageData = UIImageJPEGRepresentation(resizedImage, THUMBQUALITY);
        
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%i_thumb.jpg\"\r\n", treeId] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%d", [body length]] forHTTPHeaderField:@"Content-Length"];
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    // full size
    UIGraphicsBeginImageContext(fullSize);
    [photo drawInRect:CGRectMake(0,0,fullSize.width,fullSize.height)];
    resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    imageData = UIImageJPEGRepresentation(resizedImage, FULLQUALITY);

    body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"userfile\"; filename=\"%i.jpg\"\r\n", treeId] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    [request addValue:[NSString stringWithFormat:@"%d", [body length]] forHTTPHeaderField:@"Content-Length"];
    
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];

}

- (NSDictionary *)getTreeBySite:(NSString *)site
{
    NSArray *trees = nil;
    NSString *urlstring = [NSString stringWithFormat:@"http://www7.pairlite.com/pl1705/x/vgtreedb/trees.php?method=getTrees&site=%@", site];
    NSURL *url = [NSURL URLWithString:urlstring];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:apikey forHTTPHeaderField:@"Authorization"];
    NSHTTPURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (responseData != nil) {
        //NSLog(@"getCourts response: %i\n%@", response.statusCode, [NSString stringWithCString:[responseData bytes] encoding:NSUTF8StringEncoding]);
        trees = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
    }
    
    return [trees objectAtIndex:0];
}

@end
