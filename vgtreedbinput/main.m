//
//  main.m
//  vgtreedbinput
//
//  Created by Steven Kim on 1/22/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VGAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VGAppDelegate class]));
    }
}
