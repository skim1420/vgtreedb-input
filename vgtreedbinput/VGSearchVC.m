//
//  VGTagVC.m
//  vgtreedbinput
//
//  Created by Steven Kim on 3/17/13.
//  Copyright (c) 2013 Vane Software. All rights reserved.
//

#import "VGSearchVC.h"
#import "VGTreeDetailsVC.h"

@interface VGSearchVC ()

@property (weak, nonatomic) IBOutlet UITextField *treeTag;

@end

@implementation VGSearchVC

@synthesize vgapi = _vgapi;
@synthesize treeTag = _treeTag;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.treeTag becomeFirstResponder];
    NSLog(@"viewdidload");
}


- (void)viewDidUnload
{
    [self setTreeTag:nil];
    [super viewDidUnload];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Tree By Site"]) {
        NSLog(@"here");
        NSDictionary *tree = [self.vgapi getTreeBySite:self.treeTag.text];
        [segue.destinationViewController setTree:tree];
    }
}

- (IBAction)findTree:(id)sender
{
    NSLog(@"about to seg..");
    [self performSegueWithIdentifier:@"Tree By Site" sender:self];
}

@end
